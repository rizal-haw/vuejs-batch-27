
// SOAL NO 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

// ..... Jawaban SOAL NO 1

var kata1 = pertama.substr(0, 4)
var kata2 = pertama.substr(12,6)

var kata3 = kedua.substr(0, 7)
var kata4 = kedua.substr(8, 10)
var kapital = kata4.toUpperCase()
// Gabung
var gabung = kata1.concat(" ", kata2, " ", kata3, " ", kapital)
console.log(gabung)


// SOAL NO 2

var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

// .....Jawaban SOAL NO 2

var a = parseInt(kataPertama)
var b = parseInt(kataKedua)
var c = parseInt(kataKetiga)
var d = parseInt(kataKeempat)

var aritmatika = a + b * c + d

console.log(aritmatika)

// SOAL NO 3

var kalimat = 'wah javascript itu keren sekali'; 

// ..... Jawaban SOAL NO 3

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); 
var kataKetiga = kalimat.substring(14,18); 
var kataKeempat = kalimat.substring(19, 24); 
var kataKelima = kalimat.substring(25, 31); 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);