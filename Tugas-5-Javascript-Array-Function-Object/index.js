// SOAL NO 1

/*var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
urutkan array di atas dan tampilkan data seperti output di bawah ini (dengan menggunakan loop):*/

// ......JAWABAN NO 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
var str = ""
daftarHewan.sort()
for (i = 0; i < daftarHewan.length; i++) {
    str += daftarHewan[i] + "\n";
   }
console.log(str)

// SOAL NO 2

/* Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: "Nama saya [name], umur saya [age] tahun, alamat saya di [address], dan saya punya hobby yaitu [hobby]! */

// ..........JAWABAN NO 2
function introduce(perkenalan) {
    var perkenalan = "Nama saya " + perkenalan.name + ", umur saya " + perkenalan.age + " tahun, alamat saya di " + perkenalan.address + ", dan saya punya hobby yaitu " + perkenalan.hobby
    return perkenalan;
}

var data = {name : "Rizal" , age : 24 , address : "Jalan Kresna No 22" , hobby : "Writing" }
 
var perkenalan = introduce(data)
console.log(perkenalan)

// SOAL NO 3

/*Tulislah sebuah function dengan nama hitung_huruf_vokal() yang menerima parameter sebuah string, kemudian memproses tersebut sehingga menghasilkan total jumlah huruf vokal dalam string tersebut.*/

function hitung_huruf_vokal(huruf){
    var listVokal = "aiueoAIUEO"
    var hitung = 0;

    for(var i = 0; i < huruf.length; i++){
        if (listVokal.indexOf(huruf[i]) !== -1){
            hitung +=1;
        }
    }
    return hitung;
}

var hitung_1 = hitung_huruf_vokal("Muhammad")

var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2)

// SOAL NO 4

/*Buatlah sebuah function dengan nama hitung() yang menerima parameter sebuah integer dan mengembalikan nilai sebuah integer, dengan contoh input dan otput sebagai berikut.*/

// ......JAWAAN NO 4
function hitung(int){
    var operasi = int * 2 - 2;
    return operasi;
}

console.log(hitung(0))
console.log(hitung(1))
console.log(hitung(2))
console.log(hitung(3))
console.log(hitung(5))